﻿cmake_minimum_required (VERSION 3.8)
set( CMAKE_C_STANDARD 99 )

project( Gateway LANGUAGES C CXX )

set(source_files
                                src/main.cpp
                                src/server/server.cpp
                                src/server/pubsub_configs.cpp

)

set(header_files
                                src/server/server.h
                                src/server/pubsub_configs.h
)

# PUBSUB Switches
set (UA_ENABLE_PUBSUB ON)

add_executable(${PROJECT_NAME} WIN32 ${source_files} ${header_files})

set (git_cmd "git")
set (git_clone "clone")
execute_process(COMMAND ${git_cmd} ${git_clone} https://github.com/open62541/open62541.git "${PROJECT_SOURCE_DIR}/thirdparty/open62541"
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  RESULT_VARIABLE git_result
  OUTPUT_VARIABLE git_ver)

add_subdirectory(thirdparty/open62541)

target_link_libraries(${PROJECT_NAME} open62541::open62541)
