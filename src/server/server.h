﻿#ifndef SERVER_H
#define SERVER_H

#include <open62541/plugin/log_stdout.h>
#include <open62541/server.h>
#include <open62541/server_config_default.h>

#include <signal.h>

#include "pubsub_configs.h"

class Server {

  public:

    explicit Server() : m_pubsubConfig() {}

    explicit Server(char* hostname, int portnumber)
        : m_pubsubConfig(hostname, portnumber) {}

    static void stopHandler(int sig);

    void create();

    void preparePubSub();

    void run();

    void addPubSubConnection(char* connectionName        = (char*)"connectionName",
                             char* publisherId           = (char*)"publisherId",
                             char* hostname              = (char*)"localhost",
                             int portnumber              = 4840,
                             char* transportProfileUri   = (char*)"http://opcfoundation.org/UA-Profile/Transport/pubsub-udp-uadp",
                             char* protocol              = (char*)"udp",
                             bool pubsubEnabled          = true );
    /**
     * **PublishedDataSet handling**
     *
     * The PublishedDataSet (PDS) and PubSubConnection are the toplevel entities and
     * can exist alone. The PDS contains the collection of the published fields. All
     * other PubSub elements are directly or indirectly linked with the PDS or
     * connection. */
    void addPublishedDataSet(   char* name,
                                UA_PublishedDataSetType publishedDataSetType = UA_PUBSUB_DATASET_PUBLISHEDITEMS );

    /**
     * **DataSetField handling**
     *
     * The DataSetField (DSF) is part of the PDS and describes exactly one published
     * field.
     ** @TODO! collect the nodeid (return value) of the created fields somewhere. */
    UA_NodeId addDataSetField(  char* fieldNameAlias,
                                bool promotedField,
                                UA_NodeId publishedVariable,
                                UA_UInt32 attributeId,
                                UA_DataSetFieldType dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE  );

    /**
     * **WriterGroup handling**
     *
     * The WriterGroup (WG) is part of the connection and contains the primary
     * configuration parameters for the message creation. */
    void addWriterGroup(char* name,
                        double publishingInterval,
                        bool enabled,
                        int16_t writerGroupId,
                        UA_PubSubEncodingType encodingMimeType = UA_PUBSUB_ENCODING_UADP,
                        UA_PubSubRTLevel RTLevel = UA_PUBSUB_RT_NONE,                                                           /** REALTIME CONFIG **/
                        UA_ExtensionObjectEncoding messageSettings_encoding = UA_EXTENSIONOBJECT_DECODED,
                        UA_DataType messageSettings_content_decoded_type = UA_TYPES[UA_TYPES_UADPWRITERGROUPMESSAGEDATATYPE]  );
    /**
     * **DataSetWriter handling**
     *
     * A DataSetWriter (DSW) is the glue between the WG and the PDS. The DSW is
     * linked to exactly one PDS and contains additional information for the
     * message generation. */
    UA_NodeId addDataSetWriter( char* name,
                                int16_t dataSetWriterId = (int16_t) 62541,
                                int32_t keyFrameCount = 10);

    UA_Server* getUAServer(){ return m_UAServer; }

    UA_StatusCode getStatus(){ return m_status; }

    PubSubConfig getubsubConfig(){ return m_pubsubConfig; }

    virtual ~Server(){
        UA_Server_delete(m_UAServer);
    }

  private:
    UA_Server* m_UAServer;
    UA_ServerConfig* m_config;
    UA_StatusCode m_status;
    PubSubConfig m_pubsubConfig;

    UA_NodeId m_connectionId;       //connectionIdent
    UA_NodeId m_publishedDataSetId; //publishedDataSetIdent
    UA_NodeId m_writerGroupId;      //writerGroupIdent
};

#endif
