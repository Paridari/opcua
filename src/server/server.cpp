﻿#include "server.h"

static volatile UA_Boolean m_Running = UA_TRUE;

void Server::stopHandler(int sig) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "received ctrl-c");
    m_Running = UA_FALSE;
}

void Server::create(){
    m_UAServer = UA_Server_new();
    m_config = UA_Server_getConfig(m_UAServer);
    UA_ServerConfig_setMinimal(m_config, m_pubsubConfig.getPortnumber(), 0);
    UA_ServerConfig_setCustomHostname(m_config, m_pubsubConfig.getHostname());
}

void Server::preparePubSub(){
//    /* Details about the connection configuration and handling are located in
//     * the pubsub connection tutorial */
    UA_StatusCode sc = UA_ServerConfig_addPubSubTransportLayer(m_config, UA_PubSubTransportLayerUDPMP());
    sc == UA_STATUSCODE_GOOD ?  printf("addPubSubTransportLayer_GOOD\n") : printf("addPubSubTransportLayer_FAIL\n");
#ifdef UA_ENABLE_PUBSUB_ETH_UADP
    UA_ServerConfig_addPubSubTransportLayer(m_config, UA_PubSubTransportLayerEthernet());
#endif

    Server::addPubSubConnection("publisher connection test", "thing1.equipment1.sensor1");
    Server::addPublishedDataSet("PDS test");
    Server::addDataSetField("Server localtime",
                            false,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_SERVER_SERVERSTATUS_CURRENTTIME),
                            UA_ATTRIBUTEID_VALUE,
                            UA_PUBSUB_DATASETFIELD_VARIABLE);
    Server::addWriterGroup("WG test",
                           100,
                           false,
                           100);
    Server::addDataSetWriter("DSW test");
}

void Server::run(){
    signal(SIGINT, Server::stopHandler);
    signal(SIGTERM, Server::stopHandler);
    m_status = UA_Server_run(m_UAServer, &m_Running);
}

void Server::addPubSubConnection(char* connectionName,
                                 char* publisherId,
                                 char* hostname,
                                 int portnumber,
                                 char* transportProfileUri,
                                 char* protocol,
                                 bool pubsubEnabled){

//    memset(&m_pubsubConfig.getPubSubConnectionConfig(), 0, sizeof(m_pubsubConfig.getPubSubConnectionConfig()));

    /* Details about the connection configuration and handling are located
     * in the pubsub connection tutorial */
    m_pubsubConfig.setHostname(hostname);
    m_pubsubConfig.setPortnumber(portnumber);
    m_pubsubConfig.setConnectionName(connectionName);
    m_pubsubConfig.setPublisherId(publisherId);
    m_pubsubConfig.setTransportProfileUri(transportProfileUri);
    m_pubsubConfig.setNetworkAddressUrl(protocol);
    m_pubsubConfig.setPubSubEnabled(pubsubEnabled);
    UA_StatusCode sc = UA_Server_addPubSubConnection(m_UAServer, &m_pubsubConfig.getPubSubConnectionConfig(), &m_connectionId);
    sc == UA_STATUSCODE_GOOD ?  printf("addPubSubConnection_GOOD\n") : printf("addPubSubConnection_FAIL\n");
}

void Server::addPublishedDataSet(char* name, UA_PublishedDataSetType publishedDataSetType) {
    /* The PublishedDataSetConfig contains all necessary public
    * information for the creation of a new PublishedDataSet */
    m_pubsubConfig.setPublishedDataSetConfig(name, publishedDataSetType);
    /* Create new PublishedDataSet based on the PublishedDataSetConfig. */
    UA_Server_addPublishedDataSet(m_UAServer, &m_pubsubConfig.getPublishedDataSetConfig(), &m_publishedDataSetId);
}

UA_NodeId Server::addDataSetField(char* fieldNameAlias,
                             bool promotedField,
                             UA_NodeId publishedVariable,
                             UA_UInt32 attributeId,
                             UA_DataSetFieldType dataSetFieldType) {
    /* Add a field to the previous created PublishedDataSet */
    UA_NodeId _dataSetFieldId;
    m_pubsubConfig.setDataSetFieldConfig(fieldNameAlias,
                                         promotedField,
                                         publishedVariable,
                                         attributeId,
                                         dataSetFieldType);
    UA_Server_addDataSetField(m_UAServer,
                              m_publishedDataSetId,
                              &m_pubsubConfig.getDataSetFieldConfig(),
                              &_dataSetFieldId);
    return _dataSetFieldId;
}

void Server::addWriterGroup(char* name,
                    double publishingInterval,
                    bool enabled,
                    int16_t writerGroupId,
                    UA_PubSubEncodingType encodingMimeType,
                    UA_PubSubRTLevel RTLevel,
                    UA_ExtensionObjectEncoding messageSettings_encoding,
                    UA_DataType messageSettings_content_decoded_type){

    m_pubsubConfig.setWriterGroupConfig(name,
                                        publishingInterval,
                                        enabled,
                                        writerGroupId,
                                        encodingMimeType,
                                        RTLevel,
                                        messageSettings_encoding,
                                        messageSettings_content_decoded_type);

    UA_Server_addWriterGroup(m_UAServer, m_connectionId, &m_pubsubConfig.getWriterGroupConfig(), &m_writerGroupId);
    UA_Server_setWriterGroupOperational(m_UAServer, m_writerGroupId);
    UA_UadpWriterGroupMessageDataType_delete(m_pubsubConfig.getWriterGroupMessagePtr());
}

UA_NodeId Server::addDataSetWriter( char* name,
                                    int16_t dataSetWriterId,
                                    int32_t keyFrameCount) {
    /* We need now a DataSetWriter within the WriterGroup. This means we must
    * create a new DataSetWriterConfig and add call the addWriterGroup function. */
    UA_NodeId _dataSetWriterId;
    m_pubsubConfig.setDataSetWriterConfig(name,
                                          dataSetWriterId,
                                          keyFrameCount);
    UA_Server_addDataSetWriter(m_UAServer, m_writerGroupId, m_publishedDataSetId,
                                   &m_pubsubConfig.getDataSetWriterConfig(), &_dataSetWriterId);
    return _dataSetWriterId;
}

