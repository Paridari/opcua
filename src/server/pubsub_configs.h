﻿#ifndef PUBSUBCONFIGS_H
#define PUBSUBCONFIGS_H

#include <open62541/server.h>
#include <open62541/server_config_default.h>
#include <open62541/plugin/pubsub_ethernet.h>
#include <open62541/plugin/pubsub_udp.h>

#include <memory>

class PubSubConfig {

  public:

    explicit PubSubConfig(  char* hostname              = (char*)"localhost",
                            int portnumber              = 4840,
                            char* connectionName        = (char*)"connectionName",
                            char* publisherId           = (char*)"publisherId",
                            char* transportProfileUri   = (char*)"http://opcfoundation.org/UA-Profile/Transport/pubsub-udp-uadp",
                            char* protocol              = (char*)"udp",
                            bool pubsubEnabled          = true );

    /* m_PubSubConfig setter handles */
    void setHostname(char* hostname);
    void setPortnumber(int portnumber);
    void setConnectionName(char* connectionName);
    void setPublisherId(char* publisherId);
    void setTransportProfileUri(char* transportProfileUri);
    void setNetworkAddressUrl(char* protocol);
    void setPubSubEnabled(bool pubsubEnabled);
    /* m_publishedDataSetConfig setter handle */
    void setPublishedDataSetConfig(char* name, UA_PublishedDataSetType publishedDataSetType);
    /* m_DataSetFieldConfig setter handle */
    void setDataSetFieldConfig( char* fieldNameAlias,
                                bool promotedField,
                                UA_NodeId publishedVariable,
                                UA_UInt32 attributeId,
                                UA_DataSetFieldType dataSetFieldType);

    /* m_WriterGroupConfig setter handle */
    void setWriterGroupConfig(  char* name,
                                double publishingInterval,
                                bool enabled,
                                int16_t writerGroupId,
                                UA_PubSubEncodingType encodingMimeType,
                                UA_PubSubRTLevel RTLevel,
                                UA_ExtensionObjectEncoding messageSettings_encoding,
                                UA_DataType messageSettings_content_decoded_type);

    void setDataSetWriterConfig(char* name,
                                int16_t dataSetWriterId,
                                int32_t keyFrameCount);

    /* m_PubSubConfig getter handles */
    UA_String getHostname() const ;
    UA_UInt16 getPortnumber() const ;
    UA_String getConnectionName() const ;
    UA_String getPublisherId() const ;
    UA_String getTransportProfileUri() const ;
    UA_Variant getNetworkAddressUrl() const ;
    UA_Boolean isPubSubEnabled() const ;

    UA_PubSubConnectionConfig& getPubSubConnectionConfig() ;
    /* m_publishedDataSetConfig getter handle */
    UA_PublishedDataSetConfig& getPublishedDataSetConfig() ;
    /* m_DataSetFieldConfig setter handle */
    UA_DataSetFieldConfig& getDataSetFieldConfig() ;
    /* m_WriterGroupConfig getter handle */
    UA_WriterGroupConfig& getWriterGroupConfig() ;
    /* m_DataSetWriterConfig getter handle */
    UA_DataSetWriterConfig& getDataSetWriterConfig() ;
    /* m_WriterGroupMessagePtr getter handle */
    UA_UadpWriterGroupMessageDataType* getWriterGroupMessagePtr() ;

  private:
    UA_String m_HostName;
    UA_UInt16 m_PortNumber;

    UA_PubSubConnectionConfig m_ConnectionConfig;
    UA_PublishedDataSetConfig m_PublishedDataSetConfig;
    UA_DataSetFieldConfig m_DataSetFieldConfig;
    UA_WriterGroupConfig m_WriterGroupConfig;
    UA_DataSetWriterConfig m_DataSetWriterConfig;
    UA_UadpWriterGroupMessageDataType* m_WriterGroupMessagePtr;
};

#endif
