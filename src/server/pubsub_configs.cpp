﻿#include "pubsub_configs.h"

PubSubConfig::PubSubConfig(char* hostname,
                             int portnumber,
                             char* connectionName,
                             char* publisherId,
                             char* transportProfileUri,
                             char* protocol,
                             bool enabled){

    memset(&m_ConnectionConfig, 0, sizeof(m_ConnectionConfig));
    memset(&m_PublishedDataSetConfig, 0, sizeof(m_PublishedDataSetConfig));
    memset(&m_DataSetFieldConfig, 0, sizeof(m_DataSetFieldConfig));
    memset(&m_WriterGroupConfig, 0, sizeof(m_WriterGroupConfig));
    memset(&m_DataSetWriterConfig, 0, sizeof(m_DataSetWriterConfig));
    memset(&m_WriterGroupMessagePtr, 0, sizeof(m_WriterGroupMessagePtr));

    PubSubConfig::setHostname(hostname);
    PubSubConfig::setPortnumber(portnumber);
    PubSubConfig::setConnectionName(connectionName);
    PubSubConfig::setPublisherId(publisherId);
    PubSubConfig::setTransportProfileUri(transportProfileUri);
    PubSubConfig::setNetworkAddressUrl(protocol);
    PubSubConfig::setPubSubEnabled(enabled);
}

void PubSubConfig::setHostname(char *hostname){
    UA_String_init(&m_HostName);
    m_HostName.length = strlen(hostname);
    m_HostName.data = (UA_Byte *) hostname;
}

void PubSubConfig::setPortnumber(int portnumber){
    m_PortNumber = portnumber;
}

void PubSubConfig::setConnectionName(char *connectionName){
    UA_String_init(&m_ConnectionConfig.name);
    m_ConnectionConfig.name.length = strlen(connectionName);
    m_ConnectionConfig.name.data = (UA_Byte *) connectionName;
}

void PubSubConfig::setPublisherId(char* publisherId){
    m_ConnectionConfig.publisherIdType = UA_PUBSUB_PUBLISHERID_STRING;
    UA_String_init(&m_ConnectionConfig.publisherId.string);
    m_ConnectionConfig.publisherId.string.length = strlen(publisherId);
    m_ConnectionConfig.publisherId.string.data = (UA_Byte *) publisherId;
}

void PubSubConfig::setTransportProfileUri(char* transportProfileUri){
//    UA_String _TransportProfileUri;
    UA_String_init(&m_ConnectionConfig.transportProfileUri);
    m_ConnectionConfig.transportProfileUri.length = strlen(transportProfileUri);
    m_ConnectionConfig.transportProfileUri.data = (UA_Byte *) transportProfileUri;
}

void PubSubConfig::setNetworkAddressUrl(char* protocol){
    char _Url[2000];
    sprintf(_Url, "opc.%s://%s:%d/", protocol, m_HostName.data, m_PortNumber);
    UA_String UA_url;
    UA_String_init(&UA_url);
    UA_url.length = strlen(_Url);
    UA_url.data = (UA_Byte *) _Url;

    UA_NetworkAddressUrlDataType _NetworkAddressUrl =
            {UA_STRING_NULL , UA_url};
    UA_Variant_init(&m_ConnectionConfig.address);
    UA_Variant_setScalar(&m_ConnectionConfig.address, &_NetworkAddressUrl,
                         &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE]);
}

void PubSubConfig::setPubSubEnabled(bool enabled){
    m_ConnectionConfig.enabled = enabled ? UA_TRUE : UA_FALSE;
}

void PubSubConfig::setPublishedDataSetConfig(char* name, UA_PublishedDataSetType publishedDataSetType){
    UA_String_init(&m_PublishedDataSetConfig.name);
    m_PublishedDataSetConfig.name.length = strlen(name);
    m_PublishedDataSetConfig.name.data = (UA_Byte *) name;
    m_PublishedDataSetConfig.publishedDataSetType = publishedDataSetType;
}

void PubSubConfig::setDataSetFieldConfig(char* fieldNameAlias,
                                         bool promotedField,
                                         UA_NodeId publishedVariable,
                                         UA_UInt32 attributeId,
                                         UA_DataSetFieldType dataSetFieldType){

    UA_String_init(&m_DataSetFieldConfig.field.variable.fieldNameAlias);
    m_DataSetFieldConfig.field.variable.fieldNameAlias.length = strlen(fieldNameAlias);
    m_DataSetFieldConfig.field.variable.fieldNameAlias.data = (UA_Byte *) fieldNameAlias;
    m_DataSetFieldConfig.field.variable.promotedField = promotedField;
    m_DataSetFieldConfig.field.variable.publishParameters.publishedVariable = publishedVariable;
    m_DataSetFieldConfig.field.variable.publishParameters.attributeId = attributeId;
    m_DataSetFieldConfig.dataSetFieldType = dataSetFieldType;
}

void PubSubConfig::setWriterGroupConfig( char* name,
                                         double publishingInterval,
                                         bool enabled,
                                         int16_t writerGroupId,
                                         UA_PubSubEncodingType encodingMimeType,
                                         UA_PubSubRTLevel RTLevel,
                                         UA_ExtensionObjectEncoding messageSettings_encoding,
                                         UA_DataType messageSettings_content_decoded_type
                                         ){

    UA_String_init(&m_WriterGroupConfig.name);
    m_WriterGroupConfig.name.length = strlen(name);
    m_WriterGroupConfig.name.data = (UA_Byte *) name;
    m_WriterGroupConfig.publishingInterval = publishingInterval;
    m_WriterGroupConfig.enabled = enabled ? UA_TRUE : UA_FALSE;
    m_WriterGroupConfig.writerGroupId = writerGroupId;
    m_WriterGroupConfig.encodingMimeType = encodingMimeType;
    m_WriterGroupConfig.rtLevel = RTLevel;
    m_WriterGroupConfig.messageSettings.encoding             = messageSettings_encoding;
    m_WriterGroupConfig.messageSettings.content.decoded.type = &messageSettings_content_decoded_type;

    /*@TODO! hard code implementation of **writerGroupMessage** below
     *
    /* The configuration flags for the messages are encapsulated inside the
     * message- and transport settings extension objects. These extension
     * objects are defined by the standard. e.g.
     * UadpWriterGroupMessageDataType */
    m_WriterGroupMessagePtr = UA_UadpWriterGroupMessageDataType_new();
    /* Change message settings of writerGroup to send PublisherId,
     * WriterGroupId in GroupHeader and DataSetWriterId in PayloadHeader
     * of NetworkMessage */
    m_WriterGroupMessagePtr->networkMessageContentMask   =   (UA_UadpNetworkMessageContentMask)(UA_UADPNETWORKMESSAGECONTENTMASK_PUBLISHERID |
                                                        (UA_UadpNetworkMessageContentMask) UA_UADPNETWORKMESSAGECONTENTMASK_GROUPHEADER |
                                                        (UA_UadpNetworkMessageContentMask) UA_UADPNETWORKMESSAGECONTENTMASK_WRITERGROUPID |
                                                        (UA_UadpNetworkMessageContentMask) UA_UADPNETWORKMESSAGECONTENTMASK_PAYLOADHEADER);
    m_WriterGroupConfig.messageSettings.content.decoded.data = m_WriterGroupMessagePtr;
}

void PubSubConfig::setDataSetWriterConfig(  char* name,
                                            int16_t dataSetWriterId,
                                            int32_t keyFrameCount){

    UA_String_init(&m_DataSetWriterConfig.name);
    m_DataSetWriterConfig.name.length = strlen(name);
    m_DataSetWriterConfig.name.data = (UA_Byte *) name;
    m_DataSetWriterConfig.dataSetWriterId = dataSetWriterId;
    m_DataSetWriterConfig.keyFrameCount = keyFrameCount;
}

UA_String PubSubConfig::getHostname() const {return m_HostName;}
UA_UInt16 PubSubConfig::getPortnumber() const {return m_PortNumber;}
UA_String PubSubConfig::getConnectionName() const {return m_ConnectionConfig.name;}
UA_String PubSubConfig::getPublisherId() const {return m_ConnectionConfig.publisherId.string;}
UA_String PubSubConfig::getTransportProfileUri() const {return m_ConnectionConfig.transportProfileUri;}
UA_Variant PubSubConfig::getNetworkAddressUrl() const {return m_ConnectionConfig.address;}
UA_Boolean PubSubConfig::isPubSubEnabled() const {return m_ConnectionConfig.enabled;}

UA_PubSubConnectionConfig& PubSubConfig::getPubSubConnectionConfig() {return m_ConnectionConfig;}
UA_PublishedDataSetConfig& PubSubConfig::getPublishedDataSetConfig() {return m_PublishedDataSetConfig;}
UA_DataSetFieldConfig& PubSubConfig::getDataSetFieldConfig() {return m_DataSetFieldConfig;};
UA_WriterGroupConfig& PubSubConfig::getWriterGroupConfig() {return m_WriterGroupConfig;};
UA_DataSetWriterConfig& PubSubConfig::getDataSetWriterConfig() {return m_DataSetWriterConfig;};
UA_UadpWriterGroupMessageDataType* PubSubConfig::getWriterGroupMessagePtr() {return m_WriterGroupMessagePtr;};
