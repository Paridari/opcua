﻿#include "server/server.h"

int main(int argc, char * argv[]) {

    Server server1 = Server();
    server1.create();
    server1.preparePubSub();
    server1.run();

    return server1.getStatus() == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}
